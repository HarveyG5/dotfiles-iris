#!/bin/sh

unread="$(ls ~/.local/share/mail/harveyy@harveyy.xyz/INBOX/new 2>/dev/null | wc -l)"
unread2="$(ls ~/.local/share/mail/harveyg5@disroot.org/INBOX/new 2>/dev/null | wc -l)"

number=$(( unread + unread2 )) 2>/dev/null

printf "%d\n" $number
