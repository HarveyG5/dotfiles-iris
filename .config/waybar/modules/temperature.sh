#!/usr/bin/env bash

temp="$(sensors | grep "Package id 0: " | cut -d' ' -f5 | tr -d '+')"

printf "%s\n" "$temp"
